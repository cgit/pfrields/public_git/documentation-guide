<!-- $Id: -->
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN"
 "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd" [

<!-- *************** Bring in Fedora entities *************** -->
<!ENTITY % FEDORA-ENTITIES-EN SYSTEM "fdp-entities.ent">
%FEDORA-ENTITIES-EN;

]>

<chapter id="ch-how-modules-work">
  <title>How Modules Work</title>
  <para>Documentation modules have a specific structure that enables the
    preconfigured tools to work correctly.  Follow this structure exactly or you
    may have problems building your module.  The &FDP; build tools locate
    resources in the module and use them to build new output such as HTML or RPM
    packages.</para>
  <section id="sn-module-struct">
    <title>Structure of a Module</title>
    <para><xref linkend="ex-module-structure"/> shows a directory tree
      of an example module, excluding any <filename
	class="directory">CVS</filename> folders.  Note that this
      example document does not have branches.</para>
    <example id="ex-module-structure">
      <title>Example Module Structure</title>
      <screen><computeroutput><![CDATA[example-doc/
    |
    |-- en_US/
    |       |-- example-doc.xml
    |       |-- para.xml
    |       |-- doc-entities.xml
    |       `-- rpm-info.xml
    |
    |-- figs/
    |       |-- fedora-logo-sprite.eps
    |       `-- fedora-logo-sprite.png
    |
    |-- po/
    |       |-- LINGUAS
    |       |-- de.po
    |       |-- example-doc.pot
    |       `-- pt.po
    |
    `-- Makefile]]></computeroutput></screen>
    </example>
    <segmentedlist id="sg-module-contents">
      <title>CVS Module Contents</title>
      <segtitle>Component</segtitle>
      <segtitle>Type</segtitle>
      <segtitle>Usage Notes</segtitle>
      <seglistitem>
	<seg>Primary language directory</seg>
	<seg>required</seg>
	<seg>This is the only directory absolutely required. It is named
	  for the original language of the document, such as <filename
	    class="directory">en_US</filename> (US English).  The
	  primary language does not have to be US English; all languages
	  are supported.  This directory contains all the XML source for
	  the actual document, as well as XML source for
	  document-specific <firstterm>entities</firstterm><footnote>
	    <para>Think of an XML entity as a predefined snippet of
	      information. It can represent a chunk of XML source, or
	      simply a word or character. If the information changes, it
	      need be replaced only once, in the definition, to fix all
	      usage.</para>
	  </footnote>.</seg>
      </seglistitem>   
      <seglistitem>
	<seg>Graphics directory</seg>
	<seg>optional</seg>
	<seg>The <filename class="directory">figs/</filename> directory
	  is an optional directory where graphics for the document
	  should be stored. If graphics are screenshots that are
	  particular to a language, the <filename
	    class="directory">figs/</filename> directory can and should
	  be stored in a language directory.</seg>
      </seglistitem>
      <seglistitem>
	<seg>Translation (PO) directory</seg>
	<seg>optional</seg> 
	<seg>The <filename class="directory">po/</filename> directory
	  contains specially formatted Portable Object, or
	  <acronym>PO</acronym>, files created and used by translators.
	  The &FDP; build tools use these files to create translated
	  versions of documents. The translated documents are not stored
	  in CVS; they are created as needed from these PO files.  The
	  <filename class="directory">po/</filename> directory also
	  contains the <filename>LINGUAS</filename> file, which lists
	  the active translation <firstterm>locales</firstterm>, or
	  languages.</seg>
      </seglistitem>
      <seglistitem>
	<seg>Makefile</seg>
	<seg>required</seg>
	<seg>The <filename>Makefile</filename> controls the build
	  process. Its content is discussed in <xref
	    linkend="ex-makefile"/>.</seg>
      </seglistitem>
      <seglistitem>
	<seg><filename>rpm-info.xml</filename></seg>
	<seg>required</seg>
	<seg>The <filename>rpm-info.xml</filename> file contains
	  document specific metadata</seg>
      </seglistitem>
    </segmentedlist>
    <important>
      <title>Common Build Tools</title>
      <para>Never add the <systemitem>docs-common</systemitem> build
	tools directory to an individual module.  Special formatting in
	the module list downloads these tools when a user checks out a
	document module.  For more information, refer to <xref
      linkend="ch-getting-files-naming-modules"/>.</para>
    </important>
  </section>
  <section id="sn-build-system">
    <title>The Document Build System</title>
    <para>
      The build system can render the document into another format such
      as <abbrev>HTML</abbrev> or <abbrev>PDF</abbrev>, using
      <command>make(1)</command><footnote>
	<para>In Linux and &DISTRO; documentation, references to
	  commands often include a number inside parentheses.  This
	  number represents the section of
	  <firstterm>manpages</firstterm> that includes documentation
	  for that command.  To read the manpage for
	  <command>make(1)</command>, use the command <command>man 1
	    make</command>.</para>
      </footnote> and shell scripts.  Authors need
      <emphasis>no</emphasis> prior experience with either shell scripts
      or a <command>make(1)</command>.
    </para>
    <section id="sn-makefile">
      <title>The Document <filename>Makefile</filename></title>
      <para>
	Each individual document has its own
	<filename>Makefile</filename>, which only needs to be a few
	lines long. The document <filename>Makefile</filename> content
	is designed for cut and paste operations.
      </para>
      <para>
	<xref linkend="ex-makefile"/> below shows the whole
	<filename>Makefile</filename> for a simple document with two
	files and two translations.
      </para>
      <example id="ex-makefile">
	<title>Sample Document Makefile</title>
	<screen><![CDATA[DOCBASE         = example-doc
PRI_LANG        = en_US
DOC_ENTITIES    = doc-entities

define XMLFILES_template
XMLFILES-${1}   = ${1}/example-doc.xml \
                  ${1}/para.xml
endef

define find-makefile-common
for d in docs-common ../docs-common ../../docs-common; do 
if [ -f $$d/Makefile.common ]; then echo "$$d/Makefile.common"; break; fi; done 
endef
include $(shell $(find-makefile-common))]]></screen>
      </example>
      <para>
	Do not be concerned with some of the more complicated syntax
	such as the <command>XMLFILES_template</command> stanza.  An
	explanation for this template appears a few paragraphs
	below.</para>
      <segmentedlist id="sg-makefile-variables">
	<title>Makefile Variables</title>
	<segtitle>Variable</segtitle>
	<segtitle>Explanation</segtitle>
	<seglistitem>
	  <seg><systemitem class="macro">DOCBASE</systemitem></seg>
	  <seg>This variable contains the name for the main (parent) XML
	    document. Follow convention by naming your document after
	    the module name.</seg>
	</seglistitem>
	<seglistitem>
	  <seg><systemitem class="macro">PRI_LANG</systemitem></seg>
	  <seg>This variable contains the ISO code for the original
	    version of the document, such as
	    <systemitem>en_US</systemitem>.</seg>
	</seglistitem>
	<seglistitem>
	  <seg><systemitem class="macro">DOC_ENTITIES</systemitem></seg>
	  <seg>This variable contains a listing of any files containing
	    entity definitions.  The &FDP; uses a special XML format to
	    record document-specific entities, so they can be translated
	    and built on the fly like any other XML document.  An
	    example is shown later in this guide. <!-- need xref here
	    --></seg>
	</seglistitem>
	<seglistitem>
	  <seg><systemitem
	      class="macro">XMLFILES_template</systemitem></seg>
	  <seg>This template allows the build tools to work with the
	    document in multiple languages once it is translated.  The
	    <systemitem class="macro">${1}</systemitem> marking is a
	    variable used to substitute the appropriate language.  This
	    template is not terribly complicated.  For a new module,
	    duplicate this section exactly except for the actual
	    filenames. Prepend the text <systemitem
	      class="macro">${1}/</systemitem>, in place of the language
	    code directory name, to each filename in your document.
	  </seg>
	</seglistitem>
      </segmentedlist>
      <important>
	<title>Files Exempt From Listing</title>
	<para>Do not include the document-specific entities XML file or
	  the <filename>rpm-info.xml</filename> file, which will be
	  discussed later in this guide.<!-- include xref --></para>
      </important>
      <para>
	The final section, beginning with <literal>define</literal>,
	locates the main <filename>Makefile.common</filename> for the
	build system. This <filename>Makefile.common</filename> file
	contains all the <application>make(1)</application> targets and
	rules to actually build the document and the various archives.
      </para>
    </section>
    <section id="sn-rpm-info">
      <title>The Document <filename>rpm-info.xml</filename></title>
      <para>Each document module's primary language directory contains a
	file called <filename>rpm-info.xml</filename>.  This file
	contains document-specific metadata used to generate revision
	history, copyright, and contributor information.  It follows a
	DTD specification included with the rest of the build system
	tools.</para>
    </section>
  </section>
  <section id="ch-getting-files-build-system-targets">
    <title>Build System Actions</title>
    <para>
      To render the <abbrev>XML</abbrev> document into another format,
      use one of the following <command>make</command> targets:
    </para>
    <segmentedlist>
      <title>Build Targets</title>
      <segtitle>Target</segtitle>
      <segtitle>Explanation</segtitle>
      <seglistitem>
	<seg><systemitem class="macro">help</systemitem></seg>
	<seg>This target prints a list of available targets and
	  their descriptions.</seg>
      </seglistitem>
      <seglistitem>
	<seg><systemitem class="macro">validate-xml</systemitem></seg>
	<seg>This target <firstterm>validates</firstterm> the XML files
	  to make sure they are not only well-formed, but follow the
	  DTD.  Some viewers of XML documents may not work correctly
	  until you run this target.  This target includes copying
	  required entity files so that validating XML viewers work
	  properly</seg>
      </seglistitem>
      <seglistitem>
	<seg><systemitem class="macro">html</systemitem></seg>
	<seg>This target builds the "chunked" <abbrev>HTML</abbrev>
	  document for each defined translation.  Output is placed in a
	  separate directory named <filename
	    class="directory"><systemitem
	      class="macro">${DOCBASE}</systemitem>-<systemitem
	      class="macro">${LANG}</systemitem>/</filename>. Each
	  document section is a separate file within that
	  directory.</seg>
      </seglistitem>
      <seglistitem>
	<seg><systemitem class="macro">html-nochunks</systemitem></seg>
	<seg>This target builds the "non-chunked" <abbrev>HTML</abbrev>
	  document for each defined translation. Output is placed in a
	  single file: <filename><systemitem
	      class="macro">${DOCBASE}</systemitem>-<systemitem
	      class="macro">${LANG}</systemitem>.html</filename>; no
	  other files are created.</seg>
      </seglistitem>
      <seglistitem>
	<seg><systemitem class="macro">pdf</systemitem></seg>
	<seg>This target builds only the <abbrev>PDF</abbrev> document
	  for all document languages. <abbrev>PDF</abbrev> production is
	  currently erratic and may not work for your document.</seg>
      </seglistitem>
      <seglistitem>
	<seg><systemitem class="macro">tarball</systemitem></seg>
	<seg>This target builds only the <command>tar(1)</command>
	  archive for all document languages.</seg>
      </seglistitem>
      <seglistitem>
	<seg><systemitem class="macro">all</systemitem></seg>
	<seg>This target builds all targets listed above.</seg>
      </seglistitem>
      <seglistitem>
	<seg><systemitem class="macro">clean</systemitem></seg>
	<seg>This target deletes any temporary, or generated files, but
	  does <emphasis>not</emphasis> erase any <abbrev>HTML</abbrev>,
	  <abbrev>PDF</abbrev>, or archive files.</seg>
      </seglistitem>
      <seglistitem>
	<seg><systemitem class="macro">distclean</systemitem></seg>
	<seg>This target erases all <abbrev>HTML</abbrev>,
	  <abbrev>PDF</abbrev>, and archive files.  This target
	  automatically invokes the <filename>clean</filename> target as
	  well.</seg>
      </seglistitem>
    </segmentedlist>
    <note>
      <title>PDF Generation is Erratic</title>
      <para><abbrev>PDF</abbrev> production is currently erratic and may
	not work for your document.</para>
    </note>
  </section>
    <section id="sn-module-adding-targets">
      <title>Adding or Changing Targets</title>
      <para>
        To add a new target and rules, place them at the bottom of the
	document <filename>Makefile</filename>, below the
	<literal>include</literal> line.  Follow your target definitions
	with a double colon, not a single colon.  The double colon
	allows you to specify additional rules for existing targets, or
	to write rules for a new target.
      </para>
      <para>
	For more information on using <command>make(1)</command>,
	consult the online documentation with the command <command>info
	  make</command> in a terminal.
      </para>
    </section>
    <section id="ch-getting-files-build-system-images">
      <title>Using Document Image Files</title>
      <para>
        Tutorial documents often make use of images such as <filename
	  class="extension">.PNG</filename> files.  Store image files in
	a <filename class="directory">figs/</filename> folder in the
	main module directory, as shown in <xref
	  linkend="ex-module-structure"/>.
      </para>
      <para>
	Depending on the output media, sometimes images may be scaled,
	streteched, or squashed. To minimize any distortions, we
	recommend that you use only <filename
	  class="extension">.PNG</filename> images.  Avoid <filename
	  class="extension">.JPG</filename> files.  The
	<command>convert(1)</command> program, from the <package
	  role="rpm">ImageMagick</package> <abbrev>RPM</abbrev> package,
	provides a convenient way to reformat <filename
	  class="extension">.JPG</filename> images into <filename
	class="extension">.PNG</filename> format.  For more information
	on formatting images such as screenshots, refer to <xref
	linkend="sn-screenshots"/>.
      </para>
      <para>
        Image files may be organized into subdirectories under
	<filename>figs/</filename> if necessary. The document building
	system recreates the image subdirectory structure in the output
	documents.
      </para>
      <para>
	Images often contain labels or other text which may need to be
	localized.  A screenshot of a program, for example, may require
	a version for each translated language.  Name language-dependent
	image files such as program screenshots by adding the language
	code to the filename, such as
	<filename>menu-en_US.png</filename>.  Language-independent
	images, such as <filename>icon.png</filename>, do not need
	language codes.
      </para>
      <para>
        Sometimes, a document may require images that do not follow the
	naming convention.  To use these images with the document
	building system, create an ordinary text file containing the
	image filenames.  This file must be named
	<filename>figs/Manifest-</filename><systemitem
	  class="macro">${LANG}</systemitem> so the build system finds
	it when searching for image filenames.
      </para>
      <para>
	<xref linkend="ch-getting-files-build-system-manifest"/>
	demonstrates one way to create this
	<filename>Manifest</filename> file.
      </para>
      <example id="ch-getting-files-build-system-manifest">
	<title>Building A Manifest</title>
	<programlisting><![CDATA[rm -f figs/Manifest-en
find figs -print >/tmp/manifest
mv /tmp/manifest figs/Manifest-en]]></programlisting>
      </example>
    </section>
    <section id="sn-adding-docbook-file">
      <title>Adding a New DocBook XML File</title>
      <para>To add a new DocBook XML file to an existing document,
	follow these steps:</para>
      <procedure>
	<step>
	  <para>Place the new DocBook XML file in the primary language
	    directory.</para>
	</step>
	<step>
	  <para>Edit the <filename>Makefile</filename> and add the
	    filename to the <varname>XMLFILES-${1}</varname> listing.
	    Append a <keycap>\</keycap> to the last existing line, and
	    on the next line add an entry for the new file.  Remember to
	    add the <literal>${1}/</literal> prefix as a substitute for
	    the language directory name.</para>
	</step>
      </procedure>
    </section>
    <section id="sn-adding-translation">
      <title>Adding a Translation</title>
      <para>Translations are stored as PO (portable object) files, which
	the toolchain transforms into translated documents.  Each PO
	file is based on the POT (PO template) for the document and
	translated by the &FED; Translation Project.  To add a
	translation, follow these steps:</para>
      <procedure>
	<step>
	  <para>If the <filename class="directory">po/</filename>
	    directory does not exist, create it and add it to
	    CVS:</para>
	  <screen><![CDATA[mkdir po && cvs add po/]]></screen>
	</step>
	<step>
	  <para>If it does not exist, create the POT file:</para>
	  <screen><![CDATA[make pot]]></screen>
	</step>
	<step>
	  <para>Add the new translation language to the
	    <varname>OTHERS</varname> listing in the
	    <filename>Makefile</filename>.</para>
	</step>
	<step>
	  <para>Although translators often copy the POT manually to
	    create the new PO file, the following command also
	    works:</para>
	  <screen>make po/<replaceable>lang</replaceable>.po</screen>
	</step>
      </procedure>
    </section>
</chapter>

<!--
Local variables:
mode: xml
fill-column: 72
End:
-->
