#######################################################################
# Makefile for Fedora Documentation Project
# License: GPL
# Copyright 2003-2006 Tammy Fox, Red Hat, Inc., Tommy Reynolds, 
# Paul W. Frields
#######################################################################

PRI_LANG	       	= en_US
DOCBASE 	       	= documentation-guide

define	XMLFILES_template
XMLFILES-${1}=		${1}/acknowledgments.xml			\
			${1}/emacs-nxml.xml				\
			${1}/emacs.xml					\
			${1}/getting-files.xml				\
			${1}/intro.xml					\
			${1}/module-struct.xml				\
			${1}/writing-guidelines.xml			\
			${1}/style.xml					\
			${1}/tutorial.xml				\
			${1}/vim.xml					\
			${1}/publishing.xml				\
			${1}/l10n.xml					\
			${1}/${DOCBASE}.xml
endef


######################################################
define find-makefile-common 
for d in docs-common ../docs-common ../../docs-common; do \
if [ -f $$d/Makefile.common ]; then echo "$$d/Makefile.common"; break; fi; done 
endef 
include $(shell $(find-makefile-common))
######################################################
